import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionComponent } from './component/connexion/connexion.component';
import { InscriptionComponent } from './component/inscription/inscription.component';
import { SaisiePoidsComponent } from './component/saisie-poids/saisie-poids.component';
import { TrimestreComponent } from './component/trimestre/trimestre.component';
import { AuthService } from './service/auth.service';
import { CardComponent } from './shared/card/card.component';
import { Cardv2Component } from './shared/cardv2/cardv2.component';
import { MoisComponent } from './component/mois/mois.component';


const routes: Routes = [
  {path:"",component:InscriptionComponent},
  {path:"inscription",component:InscriptionComponent},
  {path:"connexion",component:ConnexionComponent},
  {path:"semaine-v-1",component:CardComponent,canActivate:[AuthService]},
  {path:"semaine-v-2",component:Cardv2Component,canActivate:[AuthService]},
  {path:"saisie-poids",component:SaisiePoidsComponent,canActivate:[AuthService]},
  {path:"mois",component:MoisComponent,canActivate:[AuthService]},
  {path:"trimestre",component:TrimestreComponent,canActivate:[AuthService]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
