import { Component } from '@angular/core';
import { LoginService } from './service/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app-imc';

  tabUserPoids:any[] = [];
  constructor(private loginService: LoginService){}
  isLog(){
    return this.loginService.isLog()
  }
  receiveChildData($event:any) {
    this.tabUserPoids = $event;
  }

}
