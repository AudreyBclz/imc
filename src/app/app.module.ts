import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConnexionComponent } from './component/connexion/connexion.component';
import { NavsideComponent } from './shared/navside/navside.component';
import { NgxGaugeModule } from 'ngx-gauge';
import { CardComponent } from './shared/card/card.component';
import { Cardv2Component } from './shared/cardv2/cardv2.component';
import { InscriptionComponent } from './component/inscription/inscription.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SaisiePoidsComponent } from './component/saisie-poids/saisie-poids.component';
import { MoisComponent } from './component/mois/mois.component';
import { GraphiqueComponent } from './shared/graphique/graphique.component';
import { ImcPipe } from './pipe/imc.pipe';
import { TrimestreComponent } from './component/trimestre/trimestre.component';

import * as CanvasJSAngularChart from '../assets/canvasjs.angular.component';
var CanvasJSChart = CanvasJSAngularChart.CanvasJSChart;

@NgModule({
  declarations: [
    AppComponent,
    ConnexionComponent,
    NavsideComponent,
    CardComponent,
    Cardv2Component,
    InscriptionComponent,
    SaisiePoidsComponent,
    MoisComponent,
    CanvasJSChart,
    GraphiqueComponent,
    ImcPipe,
    TrimestreComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgxGaugeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
