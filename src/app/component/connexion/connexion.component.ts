import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
  alerte:string="";
  result:any;
form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router:Router,
    private loginService:LoginService
      ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [''],
      name
        : this.fb.control('', Validators.required),
      password: this.fb.control('', [
        Validators.required,
        Validators.pattern(
          '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
        ),
      ]),
    });
  }
  get name(){
    return this.form.get('name');
  }
  get password(){
    return this.form.get('password');
  }

  connecter(){
    if(this.form.valid){
      let user ={
        name:this.form.get('name').value,
        password:this.form.get('password').value
      }
      this.userService.connect(user).subscribe(
        res => { this.result = res; this.loginService.connect(this.result.body.data.id,this.result.body.data.taille,this.result.body.data.token); this.router.navigate(['/semaine-v-1']);console.log(this.result.body.data);},
      err => {
          this.alerte = err.error.message;
          setTimeout(() => {
            this.alerte=""
          }, 3000);
          console.log('error :'+err.error.message)},
      () => console.log('HTTP request completed.'))
    }
  }
}
