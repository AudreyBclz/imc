import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';


@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css'],
})
export class InscriptionComponent implements OnInit {
  form: FormGroup;

  alerte:string="";
  constructor(private fb: FormBuilder,
    private userService: UserService,
    private router: Router

  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [''],
      name: this.fb.control('', Validators.required),
      password: this.fb.control('', [
        Validators.required,
        Validators.pattern(
          '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
        ),
      ]),
      age: this.fb.control('', [Validators.required, Validators.min(16), Validators.max(124)]),
      taille: this.fb.control('', [Validators.required, Validators.min(100), Validators.max(250)]),
      poids: this.fb.control('', [Validators.required, Validators.min(40), Validators.max(200)]),
    });
  }
  get name() {
    return this.form.get('name');
  }
  get password() {
    return this.form.get('password');
  }
  get age() {
    return this.form.get('age');
  }
  get taille() {
    return this.form.get('taille');
  }
  get poids() {
    return this.form.get('poids');
  }

  valider() {
    if (this.form.valid) {
      let user = {
        name: this.form.get('name').value,
        age: this.form.get('age').value,
        taille: this.form.get('taille').value,
        password: this.form.get('password').value,
        poids: this.form.get('poids').value,
      }
      this.userService.create(user).subscribe(
        res => { this.router.navigate(['/connexion']);console.log('HTTP response', res)},
        err => {this.alerte=err.error.message;setTimeout(()=>{
          this.alerte=""
        },3000)},
        () => console.log('HTTP request completed.')
      )
    }
  }


}
