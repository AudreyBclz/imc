import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MoisService } from 'src/app/service/mois.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-mois',
  templateUrl: './mois.component.html',
  styleUrls: ['./mois.component.css'],
})
export class MoisComponent implements OnInit {
  user: any[] = [];

  id: number = JSON.parse(localStorage.getItem('isLog')).id;
  poids: any;
  router: any;
  resultUser:any;
  taille:number;
  constructor(private moisService: MoisService, private userService: UserService) {}

  ngOnInit(): void {
    this.userService.findAUser(this.id).subscribe(
      res=>{
        this.resultUser=res;
        this.taille=this.resultUser.data.taille;
      }
    );

    this.getMois();
  }

  getMois() {
    console.log('test si ça fonctonne');

    this.moisService.getPoids(this.id).subscribe((res) => {
      this.poids = res;
      console.log(this.poids);
    });
  }


  deletePoids(id:number){
this.moisService.deletePoids(id).subscribe((res)=>{

console.log('réussi'+ res);
})
this.getMois();


  }
}
