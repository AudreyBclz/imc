import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Poids } from 'src/app/model/poids';
import { PoidsService } from 'src/app/service/poids.service';

@Component({
  selector: 'app-saisie-poids',
  templateUrl: './saisie-poids.component.html',
  styleUrls: ['./saisie-poids.component.css'],
})
export class SaisiePoidsComponent implements OnInit {
  result:any;
  valeur: string = '16px';
  poidGroup: FormGroup;
  liste : Poids[]=[];
  poids1 : Poids;
  alerte:string="";
  now:Date=new Date();
  jour:number=this.now.getDate();
  mois:number=this.now.getMonth()+1;
  annee:number=this.now.getFullYear();
  nowString:string= this.annee.toString()+'-'+this.mois.toString()+'-'+this.jour.toString();


  constructor(private fb: FormBuilder, private poidsService: PoidsService
    ) {}

  ngOnInit(): void {
    this.poidGroup = this.fb.group({
      id: [''],
      poids:this.fb.control('',[Validators.required,Validators.min(40),Validators.max(200)]),
      date: this.fb.control('',Validators.required)
    });
  }

  get poids(){
    return this.poidGroup.get('poids');
  };
  get date(){
    return this.poidGroup.get('date');
  }

  change() {
    this.valeur = '10px';
  }
  change1() {
    this.valeur = '16px';
  }
  change2() {
    this.valeur = '20px';
  }

  valider(){
    if(new Date(this.poidGroup.get('date').value)>this.now){
      this.alerte="La date est supérieure à aujourd'hui. Réessayer";
    }
    if(this.poidGroup.valid){
      let objPoids:Poids={
        idUser: JSON.parse(localStorage.getItem('isLog')).id,
        poids: this.poidGroup.get('poids').value,
        createdAt: this.poidGroup.get('date').value,
        lenght: 0
      }
      this.poidsService.addPoids(objPoids).subscribe(
      res => {
        this.result = res;
        this.alerte = this.result.message;
        console.log('HTTP response', res)},
      err => {this.alerte=err.error.message;setTimeout(()=>{
        this.alerte=""
      },3000)},
      () => console.log('HTTP request completed.'))
    }
   
  }
}
