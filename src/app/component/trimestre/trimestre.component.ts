import { Component } from '@angular/core';
import { PoidsService } from 'src/app/service/poids.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './trimestre.component.html',
  styleUrls: ['./trimestre.component.css'],
})
export class TrimestreComponent {
  chart: any;

  chartOptions: any;
  id: number = JSON.parse(localStorage.getItem('isLog')).id;
  resultPoids: any;
  resultUser: any;
  taille: number=JSON.parse(localStorage.getItem('isLog')).taille;
  tabPoids: any[] = [];

  nbMaigreur: number = 0;
  nbNormal: number = 0;
  nbSurpoids: number = 0;
  nbObese: number = 0;
  nbObeseM: number = 0;

  constructor(
    private poidsService: PoidsService) {}

  ngOnInit() {
    this.poidsService.findTrimesterPoids(this.id).subscribe(
      (res) => {
        this.resultPoids = res;
        console.log('HTTP response', this.resultPoids);

        for (let poids of this.resultPoids.poids) {
          var imc = Math.round(
            poids.poids / ((this.taille / 100) * (this.taille / 100))
          );


          console.log(poids.poids);
          console.log(this.taille);
          if (
            Math.round(
              poids.poids / ((this.taille / 100) * (this.taille / 100))
            ) < 18.5
          ) {
            this.nbMaigreur++;
          } else if (imc >= 18.5 && imc <= 25) {
            this.nbNormal++;
          } else if (imc > 25 && imc <= 30) {
            this.nbSurpoids++;
          } else if (imc >30 && imc < 40) {
            this.nbObese++;
          } else {
            this.nbObeseM++;
          }
        }

        let longueur = this.resultPoids.poids.length;
        console.log(
          this.nbMaigreur,
          this.nbNormal,
          this.nbSurpoids,
          this.nbObese,
          this.nbObeseM,
          longueur
        );
        if (this.nbMaigreur != 0) {
          this.tabPoids.push({
            y: (this.nbMaigreur / longueur) * 100,
            name: 'Maigreur',
          });
        }
        if (this.nbNormal != 0) {
          this.tabPoids.push({
            y: (this.nbNormal / longueur) * 100,
            name: 'Normal',
          });
        }
        if (this.nbSurpoids != 0) {
          this.tabPoids.push({
            y: (this.nbSurpoids / longueur) * 100,
            name: 'Surpoids',
          });
        }
        if (this.nbObese != 0) {
          this.tabPoids.push({
            y: (this.nbObese / longueur) * 100,
            name: 'Obésité',
          });
        }
        if (this.nbObeseM != 0) {
          this.tabPoids.push({
            y: (this.nbObeseM / longueur) * 100,
            name: 'Obésité morbide',
          });
        }

        this.chartOptions = {
          animationEnabled: true,
          theme: 'dark2',
          title: {
            text: 'IMC Trimestre',
          },
          data: [
            {
              type: 'pie',
              startAngle: 45,
              indexLabel: '{name}: {y}',
              indexLabelPlacement: 'inside',
              yValueFormatString: "#,###.##'%'",
              dataPoints: this.tabPoids,
            },
          ],
        };
        this.nbMaigreur = 0;
        this.nbNormal = 0;
        this.nbSurpoids = 0;
        this.nbObese = 0;
        this.nbObeseM = 0;
      },
      () => console.log('HTTP request completed.')
    );
  }
}
