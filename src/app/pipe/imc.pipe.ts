import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imc'
})
export class ImcPipe implements PipeTransform {

  transform(value:number): string[] {
    let tab:string[];
    if(value<18.5){
      tab= [
        "col-1 bg-info height-custom margin-custom",
        "height-custom margin-custom",
        "height-custom margin-custom",
        "height-custom margin-custom",
        "height-custom margin-custom",
        "maigreur",
        'bg-info',
        "Poids trop faible, réserves insuffisantes en cas de maladie"
      ]
    }else if(value>=18.5 && value<25){
      tab= [
        "col-1 bg-success height-custom margin-custom",
        "col-1 bg-success height-custom margin-custom",
        "col-1 height-custom margin-custom",
        "col-1 height-custom margin-custom",
        "col-1 height-custom margin-custom",
        "normal",
        "bg-success",
        " Poids santé ou poids idéal: essayez d'y rester !"
      ]
    }else if(value>=25 && value<30){
      tab= [
        "col-1 bg-warning height-custom margin-custom",
        "col-1 bg-warning height-custom margin-custom",
        "col-1 bg-warning height-custom margin-custom",
        "col-1 height-custom margin-custom",
        "col-1 height-custom margin-custom",
        "surpoids",
        "bg-warning",
        "Il vous est conseillé de maigrir pour atteindre un poids santé"
      ]
    }else if(value>=30 && value<40){
     tab= [
      "col-1 bg-danger height-custom margin-custom",
      "col-1 bg-danger height-custom margin-custom",
      "col-1 bg-danger height-custom margin-custom",
      "col-1 bg-danger height-custom margin-custom",
      "col-1 height-custom margin-custom",
      "obésité",
      "bg-danger",
      "Très forte probabilité d'apparation de diabète, de maladies cardiovasculaires ..."
    ]
    }else{
      tab= [
        "col-1 bg-dark height-custom margin-custom",
        "col-1 bg-dark height-custom margin-custom",
        "col-1 bg-dark height-custom margin-custom",
        "col-1 bg-dark height-custom margin-custom",
        "col-1 bg-dark height-custom margin-custom",
        "obésité morbide",
        "bg-dark",
        "De gros problèmes de santé, faîtes attention à vous."
      ]
    }
    return tab;
    }

}
