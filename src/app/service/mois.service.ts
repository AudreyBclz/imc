import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Poids } from '../model/poids';

@Injectable({
  providedIn: 'root'
})
export class MoisService {

  url: string='http://localhost:4000/api/user/poids/mois'
  url2 :string='http://localhost:4000/api/user/poids'

  constructor(private http: HttpClient) { }

  getPoids(id:number){
  return this.http.get(this.url+'/'+id).pipe();

}

deletePoids(id:number){
  return this.http.delete(this.url2+'/'+id).pipe();
}
}
