import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Poids } from '../model/poids';

@Injectable({
  providedIn: 'root'
})
export class PoidsService {

  url:string='http://localhost:4000/api/user/poids';
  constructor(private http: HttpClient) { }

  addPoids(data:any){
    return this.http.post<Poids>(this.url,data);
  }
  lastPoids(id:number){
    return this.http.get<Poids>(this.url+'/last/'+id);
  }
  findWeekPoids(id:number){
    return this.http.get<Object>(this.url+'/semaine/'+id);
  }
  findTrimesterPoids(id:number):Observable<any>{
    return this.http.get<Object>(this.url+'/trimestre/'+id)
  }
}
