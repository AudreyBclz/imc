import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = 'http://localhost:4000/api/user';
  constructor(private http: HttpClient) { }

  create(data: any) {
    return this.http.post<User>(this.baseUrl + '/inscription', data).pipe(
      map(data => console.log(data))
    );
  }
  connect(data:any){
     const httpOptions : {headers: any, observe:any,responseType:any} = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe: 'response',
      responseType:'json'
    }
    return this.http.post(this.baseUrl+'/connexion',data,httpOptions).pipe(map((data) =>{return data}))

  }
  findAUser(id:number){
    return this.http.get(this.baseUrl+'/'+id)
  }

}
