import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-camenber',
  templateUrl: './camenber.component.html',
  styleUrls: ['./camenber.component.css']
})
export class CamenberComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  chartOptions = {
	  animationEnabled: true,
	  title:{
		text: "Project Cost Breakdown"
	  },
	  data: [{
		type: "doughnut",
		yValueFormatString: "#,###.##'%'",
		indexLabel: "{name}",
		dataPoints: [
		  { y: 28, name: "Labour" },
		  { y: 10, name: "Legal" },
		  { y: 20, name: "Production" },
		  { y: 15, name: "License" },
		  { y: 23, name: "Facilities" },
		  { y: 17, name: "Taxes" },
		  { y: 12, name: "Insurance" }
		]
	  }]
	}	

}
