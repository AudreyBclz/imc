import { Component, OnInit } from '@angular/core';
import { PoidsService } from 'src/app/service/poids.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  id:number=JSON.parse(localStorage.getItem('isLog')).id;
  resultPoids:any;
  resultUser:any;
  taille:number;
  constructor(
    private poidsService: PoidsService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.poidsService.findWeekPoids(this.id).subscribe(
      res => { this.resultPoids = res;console.log('HTTP response', res)},
      () => console.log('HTTP request completed.')
    );
    this.userService.findAUser(this.id).subscribe(
      res => {
        this.resultUser = res;
        this.taille = this.resultUser.data.taille;
        console.log('HTTP response', res);
      },
      () => console.log('HTTP request completed.')
    )}

    


}
