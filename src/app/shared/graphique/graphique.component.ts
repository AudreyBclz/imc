import { Component, OnInit } from '@angular/core';
import { MoisService } from 'src/app/service/mois.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-graphique',
  templateUrl: './graphique.component.html',
  styleUrls: ['./graphique.component.css'],
})
export class GraphiqueComponent implements OnInit {

  chart: any;
  id: number = JSON.parse(localStorage.getItem('isLog')).id;
  liste: any;
  chartOptions: any = '';
  tabPoids: any[] = [];
resultUser: any;
taille:number;
  constructor(private moisService: MoisService, private userService: UserService) {}

  ngOnInit(): void {
    this.userService.findAUser(this.id).subscribe(
      res=>{
        this.resultUser=res;
        this.taille=this.resultUser.data.taille;
      }
    );

    this.moisService.getPoids(this.id).subscribe((res) => {
      console.log('test res : ' + JSON.stringify(res));
      this.liste = res;

      console.log('ouioui ' + this.liste.poids[0].createdAt);

      for (let poids of this.liste.poids) {
        this.tabPoids.push({ x: new Date  (poids.createdAt), y: (poids.poids/(this.taille/100)**2) });
      }

      this.chartOptions = {
        theme: 'light2',
        animationEnabled: true,
        zoomEnabled: true,
        title: {
          text: 'IMC DU MOIS',
        },
        axisX: {
          valueFormatString: 'DD MMM ',
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
          },
          title: "Date",

        },
        axisY: {
          labelFormatter: (e: any) => {
            var suffixes = ['', 'K', 'M', 'B', 'T'];

            var order = Math.max(
              Math.floor(Math.log(e.value) / Math.log(1000)),
              0
            );
            if (order > suffixes.length - 1) order = suffixes.length - 1;

            var suffix = suffixes[order];
            return '' + e.value / Math.pow(1000, order) + suffix;
          },
          title: "IMC",

        },
        data: [
          {
            type: 'line',
            xValueFormatString: ' DD MM YYYY ',
            yValueFormatString: 'imc #,###.##',
            dataPoints: [...this.tabPoids],
          },
        ],
      };
    });
  }


}
