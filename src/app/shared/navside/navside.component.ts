import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Poids } from 'src/app/model/poids';
import { User } from 'src/app/model/user';
import { LoginService } from 'src/app/service/login.service';
import { PoidsService } from 'src/app/service/poids.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-navside',
  templateUrl: './navside.component.html',
  styleUrls: ['./navside.component.css']
})
export class NavsideComponent implements OnInit {

  id:number=JSON.parse(localStorage.getItem('isLog')).id;
  @Output()
  show:EventEmitter<any>=new EventEmitter();

  resultPoids?:any;
  resultUser?:any;
  user:User;
  imc:number=25;
  constructor(
    private loginService:LoginService,
    private poidsService:PoidsService,
    private userService:UserService) { }

  thresholdConfig ={
    '0':{color:'aquamarine', bgOpacity:0.2,},
    '18.5':{color:'green', bgOpacity:0.2,},
    '25.1':{color:'yellow', bgOpacity:0.2,},
    '30.1':{color:'red', bgOpacity:0.2,},
    '40':{color:'black', bgOpacity:0.2,}
  }


  markers={
    '0':
    {
      color: 'grey',
      type: 'line',
      size: 6,
      label: '0',
      font: '12px arial',
    },
    "5":
    {
      color: 'grey',
      size: 4,
      type: 'line'
    },
    '10':
    {
      color: 'grey',
      type: 'line',
      size: 6,
      label: '10',
      font: '12px arial'
    },
    "15":
    {
      color: 'grey',
      size: 4,
      type: 'line'
    },
    '20':
    {
      color: 'grey',
      type: 'line',
      size: 6,
      label: '20',
      font: '12px arial'
    },
    "25":
    {
      color: 'grey',
      size: 4,
      type: 'triangle'
    },
    '30':
    {
      color: 'grey',
      type: 'line',
      size: 6,
      label: '30',
      font: '12px arial'
    },
    "35":
    {
      color: 'grey',
      size: 4,
      type: 'line'
    },
    '40':
    {
      color: 'grey',
      type: 'line',
      size: 8,
      label: '40',
      font: '12px arial'
    },
    "45":
    {
      color: 'grey',
      size: 4,
      type: 'line'
    },
     "50":
      { color: 'grey',
        type: "line",
        size: 8,
        label: "50",
        font: "12px arial"
      },
    "55":
    {
      color: 'grey',
      size: 4,
      type: 'line'
    },
    '60':
    {
      color: 'grey',
      type: 'line',
      size: 6,
      label: '60',
      font: '12px arial'
    }
  }
  ngOnInit(): void {
    this.poidsService.lastPoids(this.id).subscribe(
      res => { this.resultPoids = res;console.log('HTTP response', res)},
      () => console.log('HTTP request completed.')
    );
    this.userService.findAUser(this.id).subscribe(
      res => {
        this.resultUser = res;
        console.log('HTTP response', res);
        if(this.resultPoids != undefined){
          this.imc = Math.round((this.resultPoids.poids.poids/((this.resultUser.data.taille/100)*(this.resultUser.data.taille/100))))
        }
      },
      () => console.log('HTTP request completed.')
    );
      this.show.emit([this.resultPoids,this.resultUser])
  };

  deconnecter(){
    this.loginService.deconnect();
  }
}
